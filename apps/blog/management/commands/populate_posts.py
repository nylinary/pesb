from typing import Any, Optional
from django.core.management.base import BaseCommand
from faker import Faker
from faker.providers import lorem
from apps.blog.models import Post
from apps.users.models import User

fake = Faker("ru_RU", use_weighting=False)
fake.add_provider(lorem)


class Command(BaseCommand):
    help = "Генерация постов"

    def handle(self, *args: Any, **options: Any) -> Optional[str]:
        for _ in range(300):
            Post.objects.create(
                author=User.objects.order_by("?")[0],
                title=fake.text(max_nb_chars=100),
                summary=fake.text(max_nb_chars=200),
                content=fake.text(max_nb_chars=1000),
            )
