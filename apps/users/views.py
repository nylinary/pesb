from django.views.generic.edit import CreateView
from .forms import RegistrationForm
from django.urls import reverse_lazy
from django.contrib.auth.views import LoginView as BaseLoginView


class RegistrationView(CreateView):
    form_class = RegistrationForm
    template_name = "users/registration.html"
    # TODO Redirect to blog list
    success_url = reverse_lazy("blog:feed")


class LoginView(BaseLoginView):
    template_name = "users/login.html"
