import json
from typing import Any, Dict

from django.db.models.query import QuerySet
from django.http import HttpRequest, HttpResponseRedirect, JsonResponse
from apps.shop import models
from django.urls import reverse
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin


class CatalogListView(generic.ListView):
    context_object_name = "product_category_list"
    queryset = models.ProductCategory.objects.filter(parent__isnull=True)
    template_name = "shop/catalog.html"


class CategoryChildrenListView(generic.ListView):
    context_object_name = "product_category_list"
    model = models.ProductCategory
    template_name = "shop/catalog.html"

    def get_queryset(self) -> QuerySet[Any]:
        return self.model.objects.get(slug=self.kwargs.get("category_slug")).children.all()


class ProductsListView(generic.ListView):
    context_object_name = "products_list"
    model = models.Product
    template_name = "shop/products.html"

    def get_queryset(self) -> QuerySet[Any]:
        return self.model.objects.filter(category__slug=self.kwargs.get("category_slug"))


class CartDetailView(LoginRequiredMixin, generic.ListView):
    context_object_name = "products_list"
    model: models.Cart = models.Cart
    template_name = "shop/cart.html"

    def get_queryset(self) -> QuerySet[Any]:
        cart, _ = self.model.objects.get_or_create(user=self.request.user)
        return models.CartProducts.objects.filter(cart=cart, quantity__gt=0)

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        objects = context[self.context_object_name]
        context["total"] = int(sum(obj.product.price * obj.quantity for obj in objects))

        context[self.context_object_name] = [(obj.product, obj.quantity) for obj in objects]

        return context


class UpdateCartView(LoginRequiredMixin, generic.View):
    def post(self, request: HttpRequest) -> JsonResponse:
        self.request = request
        body = self.get_body()

        cart, _ = models.Cart.objects.get_or_create(user=self.request.user)
        product = models.Product.objects.get(slug=body["slug"])
        cart_product, _ = models.CartProducts.objects.get_or_create(cart=cart, product=product)

        quantity = cart_product.quantity + body.get("quantity", 0)

        cart_product.quantity = quantity if quantity > 0 else 0

        if cart_product.quantity == 0:
            cart_product.delete()
        else:
            cart_product.save()

        return JsonResponse({"status": 200})

    def get_body(self) -> Dict:
        body_unicode = self.request.body.decode("utf-8")
        return json.loads(body_unicode) if body_unicode else {}


class GetUserCartProductQuantityView(LoginRequiredMixin, generic.View):
    def get(self, request: HttpRequest, slug: str) -> JsonResponse:
        product = models.Product.objects.get(slug=slug)
        if card_product := models.CartProducts.objects.filter(
            cart=models.Cart.objects.get(user=request.user), product=product
        ).first():
            quantity = card_product.quantity
        else:
            quantity = 0
        return JsonResponse({"quantity": quantity})


class CheckoutView(LoginRequiredMixin, generic.View):
    def get(self, request):
        cart = models.Cart.objects.get(user=request.user)

        if cart.products.all():
            order = models.Order.objects.create(user=request.user)

            for product in cart.products.all():
                quantity = models.CartProducts.objects.get(cart=cart, product=product).quantity
                models.OrderProducts.objects.create(order=order, product=product, quantity=quantity)

            models.CartProducts.objects.filter(cart=cart).delete()

        return HttpResponseRedirect(reverse("shop:order-list"))


class OrderListView(LoginRequiredMixin, generic.ListView):
    context_object_name = "orders_list"
    model: models.Order = models.Order
    template_name = "shop/orders.html"

    def get_queryset(self) -> QuerySet[Any]:
        if self.request.user.is_staff or self.request.user.is_superuser:
            self.template_name = "shop/orders_management.html"
            return self.model.objects.all()
        return self.model.objects.filter(user=self.request.user)

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context[self.context_object_name] = [
            (obj, self.get_order_summary(obj)) for obj in context[self.context_object_name]
        ]
        return context

    def get_order_summary(self, order: models.Order) -> str:
        order_products = models.OrderProducts.objects.filter(order=order)

        return (f"{product.product.name} * {product.quantity}" for product in order_products)
