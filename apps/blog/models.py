from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from apps.users.models import User
from utils.models import SlugModel


class Post(SlugModel):
    field_to_slug = "title"

    author = models.ForeignKey(
        User,
        on_delete=models.DO_NOTHING,
        related_name="blog_posts",
    )
    title = models.CharField(_("Title"), max_length=100)
    content = models.TextField(
        _("Content"),
        max_length=1500,
    )
    summary = models.CharField(_("Summary"), max_length=200)
    created = models.DateTimeField(_("Created"), auto_now_add=True, editable=False)
    updated = models.DateTimeField(_("Updated"), auto_now=True, editable=False)

    class Meta:
        verbose_name = _("post")
        verbose_name_plural = _("posts")
        ordering = ["-created"]

    def __str__(self) -> str:
        return self.title

    def get_absolute_url(self) -> str:
        return reverse("blog:post_detail", kwargs={"slug": str(self.slug)})
