from django.urls import path
from . import views

app_name = "blog"
urlpatterns = [
    path("", views.HomePageView.as_view()),
    path("contacts/", views.ContactsPageView.as_view(), name="contacts"),
    path("about/", views.AboutPageView.as_view(), name="about"),
    path("feed/", views.PostListView.as_view(), name="feed"),
    path("post_detail/<slug:slug>/", views.PostDetailView.as_view(), name="post_detail"),
    path("create/", views.PostCreateView.as_view(), name="post_creation"),
]
