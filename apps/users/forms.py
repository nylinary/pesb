from django.contrib.auth.forms import UserCreationForm, UsernameField, AuthenticationForm
from .models import User
from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.validators import ASCIIUsernameValidator
from django.core.validators import MinLengthValidator
from django.utils.translation import gettext_lazy as _


class RegistrationForm(UserCreationForm):
    username = forms.CharField(
        label=_("Username"),
        max_length=36,
        min_length=4,
        validators=[ASCIIUsernameValidator(), MinLengthValidator(4)],
    )
    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
        strip=False,
        help_text=_("Enter the same password as before, for verification."),
    )
    email = forms.EmailField(label=_("Email"), widget=forms.EmailInput())

    class Meta:
        model = User
        fields = ("username", "email")
        field_classes = {"username": UsernameField}


class LoginForm(AuthenticationForm):
    """Form for login page."""
