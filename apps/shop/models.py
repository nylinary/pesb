from django.db import models
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from utils.models import SlugModel
from django.core.validators import MinValueValidator


class Product(SlugModel):
    field_to_slug = "name"

    name = models.CharField(_("Name"), max_length=155)
    description = models.TextField(_("Content"), max_length=1500)
    category = models.ForeignKey("ProductCategory", on_delete=models.CASCADE)
    price = models.DecimalField(_("Price"), max_digits=9, decimal_places=2, validators=[MinValueValidator(0)])
    image = models.ImageField(_("Image"), upload_to="products", null=True, blank=True)

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)

    def clean(self) -> None:
        if self.category and self.category.children.exists():
            raise ValidationError("Can not add products to Category with child Category")

    def __str__(self) -> str:
        return self.name


class ProductCategory(SlugModel):
    field_to_slug = "name"

    parent = models.ForeignKey(
        "self", on_delete=models.CASCADE, related_name="children", verbose_name=_("Parent"), null=True, blank=True
    )
    name = models.CharField(_("Name"), max_length=155)
    description = models.TextField(_("Content"), max_length=1500)
    image = models.ImageField(_("Image"), upload_to="products_categories", null=True, blank=True)

    def __str__(self) -> str:
        return self.name

    def get_children_url(self) -> str:
        return reverse("shop:category-children", kwargs={"category_slug": str(self.slug)})

    def get_products_url(self) -> str:
        return reverse("shop:products", kwargs={"category_slug": str(self.slug)})


class Cart(models.Model):
    user = models.OneToOneField("users.User", on_delete=models.CASCADE, related_name="cart")
    products = models.ManyToManyField(Product, related_name="carts", through="CartProducts")

    def __str__(self) -> str:
        return f"Корзина клиента {self.user.username}"


class CartProducts(models.Model):
    cart = models.ForeignKey(Cart, models.CASCADE, verbose_name=_("Cart"))
    product = models.OneToOneField(
        Product,
        models.CASCADE,
        verbose_name=_("Product"),
    )
    quantity = models.IntegerField(_("Quantity"), default=0, validators=[MinValueValidator(0)])

    def __str__(self) -> str:
        return self.cart.user.username


class Order(models.Model):
    user = models.ForeignKey("users.User", on_delete=models.CASCADE, related_name="orders")
    products = models.ManyToManyField(Product, related_name="orders", through="OrderProducts")

    def __str__(self) -> str:
        return f"Заказ клиента {self.user.username} №{self.id}"


class OrderProducts(models.Model):
    order = models.ForeignKey(Order, models.CASCADE, verbose_name=_("Order"))
    product = models.ForeignKey(
        Product,
        models.CASCADE,
        verbose_name=_("Product"),
    )
    quantity = models.IntegerField(_("Quantity"), default=0, validators=[MinValueValidator(0)])

    def __str__(self) -> str:
        return self.order.user.username
