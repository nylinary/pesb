import RequestManager from "./requests.js";


let request_manager = new RequestManager();
let pluses = document.querySelectorAll(".plus");
let minuses = document.querySelectorAll(".minus");

Array.from(pluses).forEach( async function(plus) {
    plus.addEventListener("click", add_product);
  });

Array.from(minuses).forEach( async function(minus) {
    minus.addEventListener("click", remove_product);
  });

function find_sibling(element, class_name) {
    let siblings = element.parentElement.children;

    for (let i = 0; i < siblings.length; i++) {
        let sibling = siblings[i];
        if (sibling.classList.contains(class_name)) {
            return sibling;
        };
    };
};

async function add_product(event) {
    let product_info = find_sibling(event.currentTarget, "product-info");
    let product_slug = product_info.getAttribute("slug");
    let product_price = Number(product_info.getAttribute("price").replace(',', '.'));
    let total = Number(document.getElementById("total").innerText.replace(',', '.'));
    let quantity_field = find_sibling(event.currentTarget, "product-quantity");

    await request_manager.send_post("/shop/add-to-cart/", {"quantity": 1, "slug": product_slug})

    let response = await request_manager.send_get(`/shop/get-user-card-product-quantity/${product_slug}/`);
    quantity_field.innerText = response.quantity;

    let new_total = total + product_price;

    document.getElementById("total").innerText = new_total;
};
  

async function remove_product(event) {
    let target = event.currentTarget;
    let product_info = find_sibling(event.currentTarget, "product-info");
    let product_slug = product_info.getAttribute("slug");
    let quantity_field = find_sibling(target, "product-quantity");
    let total = Number(document.getElementById("total").innerText.replace(',', '.'));
    let product_price = Number(product_info.getAttribute("price").replace(',', '.'));


    await request_manager.send_post("/shop/add-to-cart/", {"quantity": -1, "slug": product_slug})
    let response = await request_manager.send_get(`/shop/get-user-card-product-quantity/${product_slug}/`);

    quantity_field.innerText = response.quantity;

    let new_total = total - product_price;

    document.getElementById("total").innerText = new_total;
};
  