export default class RequestManager {
    constructor() {
        this.csrftoken = this.getCookie('csrftoken');
    }

    async send_get(url) {
        let request = new Request(url, {method: 'GET'});
        let response = await this._wait_fetch(request);
        return response;
    };

    async send_post(url, body) {
        const request = new Request(
            url,
            {
                method: 'POST',
                headers: {'X-CSRFToken': this.csrftoken},
                mode: 'same-origin', // Do not send CSRF token to another domain.,
                body: JSON.stringify(body)
            }
        );
        let response = await this._wait_fetch(request);
        return response.context;
    };
    
    async _wait_fetch(request) {
        let response = await fetch(request);
        return response.json();
    };

    getCookie(name) {
        let cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            const cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                const cookie = cookies[i].trim();
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};
