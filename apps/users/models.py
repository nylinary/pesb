from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.validators import ASCIIUsernameValidator
from django.core.validators import MinLengthValidator


class LowercaseCharField(models.CharField):
    """
    Переопределенный CharField для конвертации строки в нижний
    регистр перед сохранением или получением данных.
    """

    def to_python(self, value):
        """
        Переводит строку в нижний регистр.
        """
        value = super().to_python(value)
        if isinstance(value, str):
            return value.lower()
        return value


class LowercaseEmailField(models.EmailField):
    """
    Переопределенный EmailField для конвертации email в нижний
    регистр перед сохранением или получением данных.
    """

    def to_python(self, value):
        """
        Переводит строку в нижний регистр.
        """
        value = super().to_python(value)
        # Check if value is a string before lowercasing.
        if isinstance(value, str):
            return value.lower()
        return value


class User(AbstractUser):
    username_validator = ASCIIUsernameValidator()
    username = LowercaseCharField(
        _("Username"),
        max_length=36,
        unique=True,
        help_text=_("Required. From 4 to 36 symbols. Only letters, digits and @/./+/-/_"),
        validators=[username_validator, MinLengthValidator(4)],
        error_messages={
            "unique": _("A user with that username already exists."),
        },
        db_index=True,
    )
    first_name = models.CharField(_("First name"), max_length=128, blank=False, db_index=True)
    last_name = models.CharField(_("Last name"), max_length=128, blank=False, db_index=True)
    middle_name = models.CharField(_("Surname"), max_length=128, blank=True, null=True, db_index=True)
    email = LowercaseEmailField(_("Email"), max_length=36, unique=True, blank=False, db_index=True)

    def __str__(self) -> str:
        return self.username

    class Meta:
        verbose_name = _("User")
        verbose_name_plural = _("Users")
