from django.urls import path
from apps.shop import views

app_name = "shop"
urlpatterns = [
    path("catalog/", view=views.CatalogListView.as_view(), name="catalog"),
    path(
        "category-children/<slug:category_slug>/",
        view=views.CategoryChildrenListView.as_view(),
        name="category-children",
    ),
    path("category-products/<slug:category_slug>/", view=views.ProductsListView.as_view(), name="products"),
    path("cart/", view=views.CartDetailView.as_view(), name="cart"),
    path("add-to-cart/", view=views.UpdateCartView.as_view(), name="add-to-cart"),
    path(
        "get-user-card-product-quantity/<slug:slug>/",
        view=views.GetUserCartProductQuantityView.as_view(),
        name="get-user-card-product-quantity",
    ),
    path("checkout/", view=views.CheckoutView.as_view(), name="checkout"),
    path("order-list/", view=views.OrderListView.as_view(), name="order-list"),
    path("order-management/", view=views.OrderListView.as_view(), name="order-management"),
]
