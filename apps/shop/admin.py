from django.contrib import admin
from apps.shop import models


@admin.register(models.ProductCategory)
class ProductCategoryAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
        "parent",
    )
    prepopulated_fields = {"slug": ("name",)}


@admin.register(models.Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
        "price",
    )
    prepopulated_fields = {"slug": ("name",)}


@admin.register(models.Cart)
class CartAdmin(admin.ModelAdmin):
    list_display = ("id", "user")


@admin.register(models.Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ("id", "user")


@admin.register(models.CartProducts)
class CartProductsAdmin(admin.ModelAdmin):
    list_display = ("id", "cart", "product", "quantity")


@admin.register(models.OrderProducts)
class OrderProductsAdmin(admin.ModelAdmin):
    list_display = ("id", "order", "product", "quantity")
