from django.views import generic
from . import models
from . import forms
from django.contrib.auth.mixins import LoginRequiredMixin


class HomePageView(generic.TemplateView):
    template_name = "blog/home.html"


class ContactsPageView(generic.TemplateView):
    template_name = "blog/contacts.html"


class AboutPageView(generic.TemplateView):
    template_name = "blog/about.html"


class PostListView(generic.ListView):
    model = models.Post
    template_name = "blog/blog.html"


class PostDetailView(generic.DetailView):
    model = models.Post
    template_name = "blog/post_detail.html"


class PostCreateView(LoginRequiredMixin, generic.CreateView):
    template_name = "blog/post_creation.html"
    form_class = forms.PostCreationForm
    model = models.Post

    def form_valid(self, form):
        if form.is_valid():
            form.instance.author = self.request.user
            return super().form_valid(form)
        else:
            return super().form_invalid(form)
