import RequestManager from "./requests.js";

document.onload = get_state();

let request_manager = new RequestManager();
let add_to_cart_links = document.querySelectorAll(".add-to-cart-link");
let pluses = document.querySelectorAll(".plus");
let minuses = document.querySelectorAll(".minus");

Array.from(pluses).forEach( async function(plus) {
    plus.addEventListener("click", add_product);
  });

Array.from(minuses).forEach( async function(minus) {
    minus.addEventListener("click", remove_product);
  });

Array.from(add_to_cart_links).forEach( async function(link) {
    link.addEventListener("click", add_to_cart);
  });

async function add_to_cart(event) {
    let link = event.currentTarget;
    let product_slug = link.getAttribute("slug");
    let quantity_selector = find_sibling(link, "quantity");
    let quantity_field = quantity_selector.querySelectorAll(".product-quantity")[0];
        
    await request_manager.send_post("/shop/add-to-cart/", {"quantity": 1, "slug": product_slug});
    
    let response = await request_manager.send_get(`/shop/get-user-card-product-quantity/${product_slug}/`);
    quantity_field.innerText = response.quantity;
    
    quantity_selector.removeAttribute("hidden"); 
    link.setAttribute("hidden", "");
};

function find_sibling(element, class_name) {
    let siblings = element.parentElement.children;

    for (let i = 0; i < siblings.length; i++) {
        let sibling = siblings[i];
        if (sibling.classList.contains(class_name)) {
            return sibling;
        };
    };
};

async function add_product(event) {
    let product_slug = find_sibling(event.currentTarget.parentElement, "add-to-cart-link").getAttribute("slug");
    let quantity_field = find_sibling(event.currentTarget, "product-quantity");

    await request_manager.send_post("/shop/add-to-cart/", {"quantity": 1, "slug": product_slug})

    let response = await request_manager.send_get(`/shop/get-user-card-product-quantity/${product_slug}/`);
    quantity_field.innerText = response.quantity;
};
  

async function remove_product(event) {
    let target = event.currentTarget;
    let add_to_cart_btn = find_sibling(target.parentElement, "add-to-cart-link");
    let product_slug = add_to_cart_btn.getAttribute("slug");
    let quantity_field = find_sibling(target, "product-quantity");

    await request_manager.send_post("/shop/add-to-cart/", {"quantity": -1, "slug": product_slug})
    let response = await request_manager.send_get(`/shop/get-user-card-product-quantity/${product_slug}/`);

    quantity_field.innerText = response.quantity;
    if (response.quantity == 0) {
        add_to_cart_btn.removeAttribute("hidden");
        target.parentElement.setAttribute("hidden", "");
    };
};
  

function get_state() {
    let request_manager = new RequestManager();
    let products = document.querySelectorAll(".product");
    Array.from(products).forEach( async function(product) {
        await set_product_state(product, request_manager);
      });
};


async function set_product_state(product, request_manager) {
    let quantity_field = product.querySelectorAll(".product-quantity")[0];
    let quantity_selector = product.querySelectorAll(".quantity")[0];
    let add_to_cart_btn = product.querySelectorAll(".add-to-cart-link")[0];
    let product_slug = add_to_cart_btn.getAttribute("slug");
    
    let url = `/shop/get-user-card-product-quantity/${product_slug}/`;
    let response = await request_manager.send_get(url);
    quantity_field.innerText = response.quantity;
    
    if (response.quantity > 0) {
        add_to_cart_btn.setAttribute("hidden", "");
        quantity_selector.removeAttribute("hidden");
    } else {
        add_to_cart_btn.removeAttribute("hidden");
        quantity_selector.setAttribute("hidden", "");
    };
};