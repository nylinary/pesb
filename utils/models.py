from django.db import models

from utils.slugify import slugify
from django.utils.crypto import get_random_string


class SlugModel(models.Model):
    field_to_slug: str

    slug = models.SlugField(
        unique=True,
        max_length=250,
    )

    def save(self, *args, **kwargs):
        self.set_slug()
        super().save(*args, **kwargs)

    def set_slug(self) -> None:
        to_slug = getattr(self, self.field_to_slug)
        self.slug = self.slug or slugify(to_slug)

        while self.__class__.objects.filter(slug=self.slug).exclude(pk=self.pk).exists():
            self.slug = self.slug + "-" + get_random_string(length=12).strip("-").lower()

    class Meta:
        abstract = True
